<?php

namespace App\CalculeHeight\Service;

/**
 * Class CalculeHeight
 * @package App\CalculeHeight\Infrastructure\Service
 */
class CalculeHeight
{
    /**
     * @var array|null
     */
    private ?array $values;

    /**
     * @var array|null
     */
    private ?array $results = null;

    /**
     * CalculeHeight constructor.
     * @param array|null $values
     */
    public function __construct(?array $values = null)
    {
        $this->values = $values;
        $this->process();
    }

    /**
     * @return $this
     */
    public function process() : self
    {
        if (!empty($this->values)) {
            foreach ($this->values as $value) {
                if (is_array($value)) {
                    foreach ($value as $height => $vector) {
                        $this->results[$height] = $this->getValue($vector, $height);
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @param array $values
     * @param int $height
     * @return int|null
     */
    public function getValue(array $values = [], int $height = 0) : ?int
    {
        if (empty($values) || $height === 0) {
            return null;
        }

        $maxValue = $max = 0;

        foreach ($values as $key => $value) {
            if ($value > $maxValue) {
                $maxValue = $value;
                $max = $key;
            }
        }

        $leftHeight = $values[0];
        $area = 0;
        for ($i = 0; $i < $max; $i++) {
            if ($leftHeight < $values[$i]) {
                $leftHeight = $values[$i];
            } else {
                $area += $leftHeight - $values[$i];
            }
        }

        $rightHeight = $values[count($values) - 1];
        for ($i = count($values) - 1; $i > $max; $i--) {
            if ($rightHeight < $values[$i]) {
                $rightHeight = $values[$i];
            } else {
                $area += $rightHeight - $values[$i];
            }
        }

        return $area;
    }

    /**
     * @param array|null $values
     * @return $this
     */
    public function setValues(?array $values): self
    {
        $this->values = $values;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getValues(): ?array
    {
        return $this->values;
    }

    /**
     * @return array|null
     */
    public function getResults(): ?array
    {
        return $this->results;
    }
}