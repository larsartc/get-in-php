<?php

namespace App\CalculeHeight\Service;

use App\Kernel;

/**
 * Class ParseFile
 * @package App\CalculeHeight\Infrastructure\Service
 */
class ParseFile
{
    public const FILES_PATH = Kernel::ROOT_PATH.'/../files';

    /**
     * @var null|array
     */
    private ?array $content = null;

    /**
     * @var string|null
     */
    private ?string $fileName;

    /**
     * ParseFile constructor.
     * @param string|null $fileName
     */
    public function __construct(?string $fileName = null)
    {
        $this->fileName = $fileName;
        $this->process();
    }

    /**
     * @return $this
     */
    public function process(): self
    {
        if ($this->fileName !== null) {
            $file = $this->getPathOfFile($this->fileName);
            if (file_exists($file)) {

                $this->content = $this->clearData($file);
            }
        }

        return $this;
    }

    /**
     * @param string $fileName
     * @return string|null
     */
    public function getPathOfFile(string $fileName) : ?string
    {
        $path = realpath(sprintf('%s/%s', self::FILES_PATH, $fileName));

        return $path ?: null;
    }

    /**
     * @param array $result
     * @param string|null $value
     */
    private function separeIndex(array &$result, ?string $value = null) : void
    {
        if ($value !== null && (bool) preg_match('/^\d+$/', $value)) {
            $result[] = (int)$value;
        }
    }

    /**
     * @param array $result
     * @param string|null $value
     * @param int $counter
     */
    private function separeRange(array &$result, ?string $value, int $counter) : void
    {
        $result = array_values($result);
        $countIn = count($result);
        $index = $result[$counter] ?? null;
        if ($index !== (int)$value) {
           $splits = preg_split('/\s+/', preg_replace('/\t+|\r+|\n+/', null, $value));

            $lastIndexValue = isset($result[$countIn-1]) ? $countIn-1 : null;
            $lastIndex = $result[$countIn-1] ?? null;

           if ($lastIndex !== null && $lastIndexValue !== null && count($splits) === $lastIndex) {
               $result[][$lastIndex] = array_map('intval', $splits);
               unset($result[$lastIndexValue]);
           }
        }
    }

    /**
     * @param string $file
     * @return array
     */
    private function clearData(string $file) : array
    {
        $content = [];
        $handle = fopen($file, "rb");

        $counter = 0;
        while (($buffer = fgets($handle, 1024)) !== false) {
            $this->separeIndex($content, $buffer);
            $this->separeRange($content, $buffer, $counter);

            $counter++;
        }

        fclose($handle);

        return $content;
    }


    /**
     * @param string|null $fileName
     * @return $this
     */
    public function setFileName(?string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getContent(): ?array
    {
        return $this->content;
    }
}
