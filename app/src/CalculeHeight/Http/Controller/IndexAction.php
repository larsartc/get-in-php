<?php

namespace App\CalculeHeight\Http\Controller;

use App\CalculeHeight\Service\CalculeHeight as CalculeHeightService;
use App\CalculeHeight\Service\ParseFile as ParseFileService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class ListAction
 * @package App\CalculeHeight\Http\Controller
 */
class IndexAction extends AbstractController
{
    /**
     * @var CalculeHeightService
     */
    private CalculeHeightService $calculeHeightService;

    private ParseFileService $parseFileService;

    /**
     * IndexAction constructor.
     * @param CalculeHeightService $calculeHeightService
     * @param ParseFileService $parseFileService
     */
    public function __construct(CalculeHeightService $calculeHeightService, ParseFileService $parseFileService)
    {
        $this->calculeHeightService = $calculeHeightService;
        $this->parseFileService = $parseFileService;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $file = $request->get('file', 'arquivo_um.test');

        $results = null;

        if (!empty($file)) {
            $parser = $this->parseFileService->setFileName($file)->process();
            $results = $this->calculeHeightService->setValues($parser->getContent())->process()->getResults();
        }

        return $this->render('calculeHeight/index.html.twig', [
            'results' => $results,
            'file' => $file
        ]);
    }
}
