#Setup

#####Requesitos: 
- Docker
- Docker Compose
- Bash
- Make ( Linux e macOS/BSD ) ou Git CLI no Windows  [Download do upgrade de Git CLI](https://github.com/havennow/upgrade-git-cli)

---

####Rodar os seguintes comandos, para iniciar o projeto:
    
1) ``make build``  - esse comando constrói os containers do Docker
2) ``make install`` - esse comando instala dependências, executa migrate e data fixture
3) ``make start`` - esse comando sobe todos containers
4) ``make shell`` - esse comando entra no shell/bash do container principal "backend", acesso ao php cli e composer  
5) ``make stop`` - esse comando para todos containers

---

## Arquivos para teste : /app/files
- Estar em formato .test
- Usa-se query string no GET com nome do arquivo